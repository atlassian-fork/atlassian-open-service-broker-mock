package com.atlassian.openservicebroker.mock.model;

import lombok.Data;
import org.springframework.cloud.servicebroker.model.CreateServiceInstanceRequest;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@Entity
public class ServiceInstance {

    @Id
    @GeneratedValue
    private Long id;

    public String instanceId;

    public String planId;

    public String organizationGuid;

    public String spaceGuid;

    @ElementCollection
    public Map<String, String> parameter;

    public String lastOperation;

    public static ServiceInstance requestToBean(CreateServiceInstanceRequest sd) {
        ServiceInstance si = new ServiceInstance();
        si.setOrganizationGuid(sd.getOrganizationGuid());
        si.setSpaceGuid(sd.getSpaceGuid());
        si.setPlanId(sd.getPlanId());
        si.setInstanceId(sd.getServiceInstanceId());
        si.setParameter(
                sd.getParameters().entrySet().stream()
                    .collect(Collectors.toMap(e -> e.getKey(), e -> String.valueOf(e.getValue())))
        );
        return si;
    }

}
