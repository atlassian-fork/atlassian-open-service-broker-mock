package com.atlassian.openservicebroker.mock.service;

import org.springframework.cloud.servicebroker.model.Catalog;
import org.springframework.cloud.servicebroker.model.Plan;
import org.springframework.cloud.servicebroker.model.ServiceDefinition;
import org.springframework.cloud.servicebroker.service.CatalogService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

@Service
public class MockCatalogService implements CatalogService {

    Catalog catalog;

    @PostConstruct
    public void constructMockCatalog() {

        List<Plan> plans = Arrays.asList(
                new Plan(
                        "default",
                        "default",
                        "Default plan"
                )
        );

        // Each service definition must implement com.atlassian.openservicebroker.mock.service.creator.MockServiceCreator.java
        catalog = new Catalog(
            Arrays.asList(
                new ServiceDefinition(
                        "service-sync",
                        "service-sync",
                        "Service Sync",
                        true,
                    plans
                ),
                new ServiceDefinition(
                        "service-async",
                        "service-async",
                        "Service Async",
                        true,
                        plans
                )
            )
        );

    }

    @Override
    public Catalog getCatalog() {
        return catalog;
    }

    @Override
    public ServiceDefinition getServiceDefinition(String serviceId) {
        return catalog.getServiceDefinitions().stream()
                .filter(s -> s.getId().equals(serviceId))
                .findFirst()
                .orElse(null);
    }
}
